<!-- 自作本物 　　ユーザー一覧JSP-->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- EL使用記述 -->

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>一覧</title>
<!-- トップ文字 -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/userlist.css">
<!-- リンク先 漏れ多い -->

</head>
<body>
	<div class="alert alert-secondary" role="alert">
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a> <!--  ログアウトサーブレットに遷移 --></li>
		</ul>
	</div>


	<div class="user_area">
		<!-- cssで指定されてる箇所 -->
		<h1 class="text-center">
			ユーザー一覧
			<!-- トップの文字 -->
		</h1>
		<form action="ShinkiTourokuServlet" method="get"
			style="text-align: right">
			<a href="ShinkiTourokuServlet" class="navbar-link logout-link">新規登録</a>
		</form>

		<form action="UserListServlet" method="post">
			<!-- ユーザーサーブレットのPostへ飛ばすform -->
			<div class="www">
				<!-- カスタマー入力フォーム -->

				<div class="form-group row">
					<!-- サーブレットのpostの中で呼ばれるname="loginid" -->
					<label for="inputtext" class="col-sm-2 col-form-label"
						style="text-align: center">ログインID</label>
					<div class="col-sm-6" style="text-align: left">
						<input type="text" class="form-control" id="inputid"
							name="loginid" placeholder="ログインID">
					</div>
				</div>

				<div class="form-group row">
					<label for="inputtext" class="col-sm-2 col-form-label"
						style="text-align: center">ユーザー名</label>
					<div class="col-sm-6" style="text-align: left">
						<input type="text" class="form-control" id="inputPassword"
							name="name" placeholder="name">
					</div>
				</div>

				<div class="form-group row">
					<%-- 一つ目の生年月日ボックスの名前= bd1 --%>
					<label for="inputtext" class="col-sm-2 col-form-label"
						style="text-align: center">生年月日</label>
					<div class="col-sm-3" style="text-align: left">
						<input type="date" class="form-control" id="inputPassword"
							name="bd1">
					</div>
					~

					<div class="col-sm-3" style="text-align: left">
						<%-- 2つ目の生年月日ボックスの名前= bd2 --%>
						<input type="date" class="form-control" id="inputPassword"
							name="bd2">
					</div>
				</div>
			</div>
			<div class="seach" style="text-align: center">
				<!-- 検索ボタン -->
				<button class="btn btn-outline-secondary" type="submit">検索</button>
			</div>
		</form>
		<%-- action="UserListServlet" method="post" へ飛ばす --%>
	</div>
	<!-- user_area終わり -->

	<hr width="full">
	<!--画面いっぱいに  横線 を引く処理) -->

	<div class="list">
		<table border="1" class="table">
			<!-- テーブル作成 -->
			<tr>
				<th>ログインID</th>
				<th>ユーザー名</th>
				<th>生年月日</th>
				<th>選択</th>
			</tr>

			<tr>
				<!-- テーブルロウ -->
				<c:forEach var="user" items="${userList}">
					<!-- UserListServletでuserDAOから取ってきたデータを代入 -->
					<tr>
						<td>${user.loginId}</td>
						<!-- td= テーブルデータのこと  -->
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<!-- TODO 未実装；ログインボタンの表示制御を行う -->

						<%-- 詳細、更新、削除ボタン(管理者以外は詳細全てと自分の更新ボタンのみ表示させる) --%>
						<!-- idが1は管理者しか居ない(Unique) -->
						<td><a class="btn btn-primary"
							href="UserDetailServlet?id=${user.id}">詳細</a> <!-- ディティールサーブレットへ -->
							<c:if test="${userInfo.loginId == 'ADMIN'}">       <!-- ログインIDがADMINならば、閉じifまでが実行される -->
								<a class="btn btn-success"href="UserUpdateServlet?id=${user.id}">更新</a>
								<a class="btn btn-danger" href="DeleteServlet?id=${user.id}">削除</a>
							</c:if> <!-- デリートサーブレットへ -->
							<c:if test="${userInfo.loginId != 'ADMIN' && userInfo.loginId == user.loginId}">  <!--  管理者でなければ自分の更新は出現させる -->


								<a class="btn btn-success"href="UserUpdateServlet?id=${user.id}">更新</a>
							</c:if>
						</td>

					</tr>
				</c:forEach>
		</table>
	</div>

</body>
</html>





