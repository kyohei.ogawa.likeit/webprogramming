<!-- index.jsp -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<!-- トップ文字 -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/login.css">
</head>


<body>
	<h1 class="text-center">ログイン画面</h1>
	<!-- トップの文字 -->

	<div class="login_area">
		<!-- cssで指定されてる箇所 -->
		<c:if test="${errMsg != null}" >     <!-- カスタムタグ -->
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		<!--   LOGINサーブレットでerrMsgに入れられてる”ログインに失敗しました”を赤い枠の中で表示させる処理がなされてる。
		  （もしもええrMsgがnull出ないなら処理を行い、そうでなければシカトされる処理の式を使うにもlibに入れるものがある） -->
		</div>
	</c:if>
	<form action="LoginServlet" method="post">
		<div class="row">
			<h4>
				<span  class="badge badge-secondary">ログインID</span>
			</h4>
			<input type="text" name="loginid"
				style="backgroundcolor: gray; width: 200px;" class="form-control"
				class="text-primary">
		</div>
		<!-- スタイルの中の色、ワイド -->

		<div class="row">
			<h4>
				<span class="badge badge-secondary">パスワード</span>
			</h4>
			<input TYPE="password" name="password"
				style="backgroundcolor: gray; width: 200px;" class="form-control"
				class="text-primary">
		</div>

		<!--  パスワードの字伏せコード -->
			<button name="lib"  type="submit">ログイン</button>
			<!-- LoginServletにデータを持ってpost返し -->
			<!-- <input type="button" onClick="location.href='user-list.html'" value="ログイン"> -->
		</form>
	</div>

</body>
</html>



