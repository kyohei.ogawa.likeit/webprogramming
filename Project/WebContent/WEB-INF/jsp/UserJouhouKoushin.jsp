<!-- ユーザー情報更新jsp -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>

<!-- ブートストラップ宣言 -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/renewal.css">
</head>

<div class="alert alert-secondary" role="alert">
	<ul class="nav navbar-nav navbar-right">
		<li class="navbar-text">${userInfo.name}さん</li>
		<li class="dropdown"><a href="LogoutServlet"
			class="navbar-link logout-link">ログアウト</a> <!--  ログアウトサーブレットに遷移 --></li>
	</ul>
</div>

<body>
<div style="text-align: center">

	<h3>ユーザー情報更新</h3>
	<p class="p"> ${name}</p>
	</div>
	<form action="UserUpdateServlet" method="post">
		<c:if test="${errMsg != null }">
			<div style="text-align: center" class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
		<!-- もしもerrmsgがnull(エラー)で無ければerrMsgを出す -->

		<input type="hidden" name="id" value="${id}">    <!-- このインプットタグのvalueはhiddenの為idはブラウザでは見えない -->

			<div class="form-group row">
				<label for="inputPassword" class="col-sm-6 col-form-label"
					style="text-align: right">パスワード</label>

				<div class="col-sm-2" style="text-align: left">
					<input type="password" class="form-control" placeholder="Password"
						name="pass1">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPassword" class="col-sm-6 col-form-label"
					style="text-align: right">パスワード(確認)</label>
				<div class="col-sm-2" style="text-align: left">
					<input type="password" class="form-control" id="inputPassword"
						name="pass2" placeholder="Password">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputtext" class="col-sm-6 col-form-label" style="text-align: right">ユーザー名</label>
				<div class="col-sm-2" style="text-align: left">
					<input type="text" class="form-control" name="name"
						value="${name}">
				</div>
			</div>
			<div class="form-group row">
				<label for="datetime" class="col-sm-6 col-form-label"
					style="text-align: right">生年月日</label>
				<div class="col-sm-2" style="text-align: left">
					<input type="date" class="form-control" name="bd"
						value="${birthDate}">
				</div>
			</div>
			<div class="seach" style="text-align: center">
				<button type="submit" >更新</button>
			</div>
	</form>
			<!-- 検索ボタン -->
			<div class="return">
				<a href=UserListServlet class="btn-text-3d">戻る</a>
			</div>

</body>
</html>


<%--
<div class="form-group row">
			<label for="inputtext" class="col-sm-6 col-form-label">ログインID</label>
			<div class="col-sm-6">
			<ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name}　さん  </li>
  			<li class="dropdown"></li>
  		  </ul>

			</div>
		</div> --%>