<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報詳細参照</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/UserDetail.css">

</head>

<div class="alert alert-secondary" role="alert">
<ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>    <!--  ログアウトサーブレットに遷移 -->
            </li>
  		  </ul>
</div>


<body>
<div class="ttt">
<h3>ユーザー情報詳細参照</h3>

<div class="po">
ログインID　　　　　　${user.loginId}
</div>

<div class="po">
ユーザー名　　　　　　${user.name}
</div>

<div class="po">
生年月日　　　　　  　${user.birthDate}
</div >

<div class="po">
登録日　　　　　　${user.createDate}
</div>

<div class="po">
更新日　　　　　　${user.updateDate}
</div>
</div>
<div class="aa">
<a href=UserListServlet class="btn-text-3d" >戻る</a>
</div>



</body>

</html>