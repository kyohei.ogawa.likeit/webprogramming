


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- 文字設定 -->
<title>ユーザー新規登録</title>
<!-- ページ名 -->
<!-- ブートストラップインポート宣言 -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link rel="stylesheet" href="css/newCostom.css">
<!-- cssの指定(newCostom) -->
</head>

<div class="alert alert-secondary" role="alert">
	<ul class="nav navbar-nav navbar-right">
		<li class="navbar-text">${userInfo.name}さん</li>
		<li class="dropdown"><a href="LogoutServlet"
			class="navbar-link logout-link">ログアウト</a> <!--  ログアウトサーブレットに遷移 --></li>
	</ul>
</div>

<body>

	<h4 style="text-align: center">ユーザー新規登録</h4>


	<div class="qqq">
		<!-- 入力するものはformで囲うこと   name属性でサーブレットと同じ名前にする事でパラメータ渡せる-->
		<form action="ShinkiTourokuServlet" method="post" style="text-align: center">
			<c:if test="${errMsg != null}">
				<!-- カスタムタグ -->
				<div class="alert alert-danger" role="alert">
					${errMsg}
					<!--（もしもeerMsgがnull出ないなら処理を行い、そうでなければシカトされる処理の式を使うにもlibに入れるものがある）-->
				</div>
			</c:if>

			<!-- 新規登録入力 -->
			<!-- ログインID -->
			<div class="form-group row">
				<label for="inputtext" class="col-sm-6 col-form-label">ログインID</label>
				<div class="col-sm-6">
					<input name="loginid" type="text" class="form-control" id="inputid"
						placeholder="ID">
					<c:if test="${Msg != null}">
						<!-- カスタムタグ -->
	               ${Msg}
			</c:if>
				</div>
			</div>

			<!-- パスワード -->
			<div class="form-group row">
				<label for="inputPassword" class="col-sm-6 col-form-label">パスワード</label>
				<div class="col-sm-6">
					<input type="password" class="form-control" id="inputPassword"
						name="password1" placeholder="Password">
				</div>
			</div>
			<!-- 確認パスワード -->
			<div class="form-group row">
				<label for="inputPassword" class="col-sm-6 col-form-label">パスワード(確認)</label>
				<div class="col-sm-6">
					<input type="password" class="form-control" id="inputPassword"
						name="password2" placeholder="Password">
				</div>
			</div>

			<!-- ユーザー名 -->
			<div class="form-group row">
				<label for="inputtext" class="col-sm-6 col-form-label">ユーザー名</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="inputName" name="name"
						placeholder="name">
				</div>
			</div>

			<!-- birthday -->
			<div class="form-group row">
				<label for="datetime" class="col-sm-6 col-form-label">生年月日</label>
				<div class="col-sm-6">
					<input type="date" class="form-control" id="inputPassword"
						name="birthday" placeholder="birthday">
				</div>
			</div>
			<!-- 登録ボタン -->
			<button type="submit">登録</button>
		</form>
	</div>
	<!-- 画面遷移する為の処理 -->


	<a href=UserListServlet class="btn-text-3d">戻る</a>
</body>
</html>
