<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/delete.css">
</head>


<div class="text-left">
<div class="alert alert-secondary" role="alert">
<ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>    <!--  ログアウトサーブレットに遷移 -->
            </li>
  		  </ul>
</div>
</div>                <!-- ここまでヘッド部分 -->

<body class="text-center">
<form action="DeleteServlet"  method="post">    <!--  ここからform -->
<input type="hidden" name="id" value="${id}">        <!-- 隠しデータ(id) )を送信する、(番号) -->


<!--  デリートサーブレットのdopostに飛ばす --%>

	<!-- 上の段(グレー部分終わり) -->
	<div class="user_del">
		<h2>ユーザー削除確認</h2>
		<h5>ログインID :${name} を本当に削除してもよろしいでしょうか?</h5>
		<!-- サーブレットでnameをいじる人の名前に初期化する。 -->
	</div>

<div class="y">

         <button type="button" onclick="history.back()" class="btn btn-outline-dark" >キャンセル</button>
      <%--  typeを指定しないとdefaultの動作でsubmitになる --%>
		<!-- そのまま一覧に遷移する -->
		<button class="btn btn-outline-danger" type="submit" >削除</button>

		<%-- 削除を押すとデータを削除して削除ができればユーザー一蘭pageに遷移 --%>
</div>
</form>                                <!-- formここまで -->
</body>
</html>