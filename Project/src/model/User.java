// Userテーブルのデータを格納するためのBeans,アクセスさせる場所

package model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {  //ビーンズ処理(dataを詰める+ 他の場所からアクセスしてくる)

	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updateDate;

	// ログインセッションを保存するためのコンストラクタ
	public User(String loginId, String name) {

		this.loginId = loginId;
		this.name = name;


	}

	public  User( String logIdData,String nameData,Date b_d,String c_d,String up) {

		this.loginId = logIdData;
		this.name = nameData;
		this.birthDate = b_d;
		this.createDate = c_d;
		this.updateDate= up;


	}

	// 全てのデータをセットするコンストラクタ
	public User(int id, String loginId, String name, Date birthDate, String password, String createDate,String updateDate) {

		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}
	//新規登録するコンスト
	public User(String loginId, String password1, String password2, String name,Date birthDate) {

		this.loginId = loginId;
		this.password = password1;
		this.password = password2;
		this.name = name;
		this.birthDate = birthDate;
	}

	//アップデート(更新)コンスト
	public User(String password1, String password2, String name,Date birthDate) {

		this.password = password1;
		this.password = password2;
		this.name = name;
		this.birthDate = birthDate;
	}
	public User(String id,String name,Date bd) {
		this.loginId = id;
		this.name = name;
		this.birthDate = bd;
	}
	
	public User( String name,Date birthDate) {    //更新(pass更新無し)

		this.name = name;
		this.birthDate = birthDate;
	}




	       //アクセサ    (getメソ  setメソ    呼び出し,セットする為のメソッド)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}





}
