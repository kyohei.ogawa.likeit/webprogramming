//dataVaseにアクセスするクラス   (DBを触る時は必ずDAOを経由する事ところ)
//必ずサーブレットから飛んでくる。
//都度使うメソ用意する。
package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class userDao {
	public User findInfo(String loginId, String password) {
		//User型の情報検索クラス
		Connection conn = null;
		try {
			conn = databaseManage.getconnection();

			String SQL = "SELECT * FROM user WHERE login_id = ? and password = ?";
			//SQL文 userテーブルのログインIDにパラメータ渡し、passwordにパラメータ2を絵渡して出力
			PreparedStatement ps = conn.prepareStatement(SQL);
			ps.setString(1, loginId);
			ps.setString(2, hashPass(password));
			ResultSet rs = ps.executeQuery();

			if (!rs.next()) {
				return null;

			}
			String logIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(logIdData, nameData);

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close(); // DB KIll
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}//finginfo終わり

	public List<User> serch(String id, String name, String bd1, String bd2) { //検索メソ
		//部分一致検索だから複数の名前が上がる可能性ある為戻り値をlistに設定
		Connection conn = null;

		List<User> SerchLIst = new ArrayList<User>();
		try {
			conn = databaseManage.getconnection();
			//sql
			String SQL = "SELECT * FROM USER WHERE login_id != 'ADMIN'";
			//uniqueな情報で管理者を特定する事、もしも同じ名前が入った場合管理者2人になってしまうから。
			//管理者を含まないnameを出力
			//SQL文に足されるSQl文	(preでないstatementの時SQL文の中に変数を直に書き込める。)
			if (!(id.equals(""))) { //ログインID(完全一致)
				SQL += " AND login_id = '" + id + "'"; //
				//%部分一致させる演算子、前のみに付ければ後方一致、後ろに付ければ前方一致前後に付ければ部分一致
			} else if (!(name.equals(""))) { //ユーザー名部分一致
				SQL += " and name like'%" + name + "%'";
			} else if (!(bd1.equals(""))) { //範囲内の日付に限る(bd1以降、bd2以前)
				SQL += " and birth_date >='" + bd1 + "'";
			} else if (!(bd2.equals(""))) {
				SQL += " and birth_date <='" + bd2 + "'";
			}
			Statement st = conn.createStatement();
			System.out.println(SQL);
			ResultSet rs = st.executeQuery(SQL);

			//以下userLIstに情報を詰める処理
			while (rs.next()) {
				int id1 = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date bd = rs.getDate("birth_date");
				String pass = rs.getString("password");
				String creDate = rs.getString("create_date");
				String update = rs.getString("update_date");
				User user = new User(id1, loginId, name1, bd, pass, creDate, update);

				SerchLIst.add(user);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return SerchLIst;

	}

	public List<User> findall() {//all info 表示
		Connection conn = null;
		List<User> userLIst = new ArrayList<User>();

		try {

			conn = databaseManage.getconnection();
			//SQL入力
			String SQL = "SELECT * FROM USER WHERE login_id != 'ADMIN'";

			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(SQL); //sqlの結果をrsに格納

			//以下userLIstに情報を詰める処理
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date bd = rs.getDate("birth_date");
				String pass = rs.getString("password");
				String creDate = rs.getString("create_date");
				String update = rs.getString("update_date");
				User user = new User(id, loginId, name, bd, pass, creDate, update);

				userLIst.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close(); //db通路閉じる
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userLIst;
	} //finfallメソ終わり

	public User User_info(int id) { //user/infoメソ

		Connection conn = databaseManage.getconnection();
		try {

			String SQL = "SELECT * FROM user WHERE id = ? "; //ここの?

			PreparedStatement ps = conn.prepareStatement(SQL);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery(); //exequte=実行

			if (!rs.next()) {
				return null;

			}
			String logIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date b_d = rs.getDate("birth_date");
			String c_d = rs.getString("create_date");
			String up = rs.getString("update_date");

			User a = new User(logIdData, nameData, b_d, c_d, up);
			return a;

		} catch (Exception e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close(); // DB KIll
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	} //user_infoメソ終わろ

	public User NewCostomor(String loginId, String pass1, String pass2, String name, String b_D) { //新規登録に使うメソ
		Connection conn = null;

		try {
			conn = databaseManage.getconnection();
			//SQL入力
			String SQL = "INSERT INTO USER(login_id,name,birth_date,create_date,update_date,password)   VALUE(?,?,?,NOW(),NOW(),?)"; //インサート

			PreparedStatement pst = conn.prepareStatement(SQL);

			pst.setString(1, loginId); //NewCostomerメソの引数になってるのをSQLのvalueの何番目に何をセットするかの処理
			pst.setString(2, name);
			pst.setString(3, b_D);
			pst.setString(4, hashPass(pass1));

			pst.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public String hashPass(String pass) { //暗号化メソ
		//ハッシュを生成したい元の文字列  String source = "暗号化対象";
		String source = pass;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";
		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		System.out.println(result);

		return result;
	}

	public User Update(int id, String name, String bd, String pass1) {       //全て更新する更新メソ
		//アップデートサーブレットで使うメソ
		Connection conn = null;

		try {
			conn = databaseManage.getconnection();
			String SQL = "UPDATE USER set name=?,birth_date=?,update_date=NOW(),password=?  WHERE id=?";
			//SQL文()EL式は記述できない、id、名前等を変数にしたい時?で値をその都度セットする。

			PreparedStatement pstm = conn.prepareStatement(SQL);
			//暗号化したパスワードを pstm.setString(3, pass1);にセットする。
			//HashPass(pass1);

			pstm.setString(1, name);
			pstm.setString(2, bd);
			pstm.setString(3, hashPass(pass1));
			pstm.setInt(4, id);

			pstm.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public User Update(int id, String name, String bd) {   //パスワードは更新されないメソ(passが空で更新すると空をhash化してしまうから)
		//アップデートサーブレットで使うメソ
		Connection conn = null;

		try {
			conn = databaseManage.getconnection();
			String SQL = "UPDATE USER set name=?,birth_date=?,update_date=NOW()  WHERE id=?";
			//SQL文()EL式は記述できない、id、名前等を変数にしたい時?で値をその都度セットする。

			PreparedStatement pstm = conn.prepareStatement(SQL);
			//暗号化したパスワードを pstm.setString(3, pass1);にセットする。
			//HashPass(pass1);

			pstm.setString(1, name);
			pstm.setString(2, bd);
			//pstm.setString(3, hashPass(pass1));
			pstm.setInt(3, id);

			pstm.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public User Delite(int id) { //データ消すメソ
		//削除メソッド
		Connection conn = null;

		try {
			conn = databaseManage.getconnection();

			//削除SQL記述
			String SQL = "DELETE from user  where id=?"; //delete文はレコード単位でしか消せないから引数は一つのみでok

			PreparedStatement pst = conn.prepareStatement(SQL);

			//データをセット(削除する人の)
			pst.setInt(1, id);

			pst.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	} //deliメソ終わり

}
