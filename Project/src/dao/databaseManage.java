package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class databaseManage {

	final private static String URL = "jdbc:mysql://localhost/"; //urlを指定
	final private static String DB_NAME = "user"; //データベースのテーブル名を初期化
	final private static String PARAMETERS = "?useUnicode=true&characterEncoding=utf8";
	final private static String USER = "root";
	final private static String PASS = "password";

	public static Connection getconnection() {
		Connection con =null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(URL+DB_NAME+PARAMETERS,USER,PASS);
			}catch (SQLException | ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	        return con;
	    }
	}