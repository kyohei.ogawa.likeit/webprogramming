

//本物   自作
package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userDao;
import model.User;

@WebServlet("/LoginServlet")

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//ログイン画面(jspに遷移させる)→formでパラメーターを生成(クライアントが作成)しこのサーブレット(dopost)に返し型にはめて次のjsp に渡す

	public LoginServlet() {
		super();
	}

	// getメソッド
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//アクセスした瞬間web_exam.jspに行く記述
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web_exam.jsp"); //遷移先を指定
		dispatcher.forward(request, response);
		//引数(クライアントの入力とお返し)を持ってdopostにフォワードしてくる
	}


	//postメソッド
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8"); //文字化け防止の記述(必ず書く)
		//web_exam.jspから送られてきたﾊﾟﾗﾒｰﾀｰ受け取る処理

		String logId = request.getParameter("loginid"); //jspからﾊﾟﾗﾒｰﾀを受け取る(変数に代入)
		String pass = request.getParameter("password");



		//		daoにデータを渡して戻り知をもらう
		userDao userDao = new userDao(); //userdaoの実体化　インスタンス
		User user = userDao.findInfo(logId, pass); //作ったUserDaoのfindByLoginInfoメソ実行→userに代入
		//ログイン成功時はUser型のインスタンス。ログイン失敗時はnull
		// テーブルに該当のデータが見つからなかった場合(ログイン失敗時)              19p②
		if (user == null) {
			//ログインに失敗した際には、リクエストスコープにエラーメッセージをセットしてログイン画面にフォワード

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインに失敗しました。");

			//リクエストスコープに[ログインに失敗しました。]をセットしてログイン画面(jsp)にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/web_exam.jsp");
			dispatcher.forward(request, response);
			return;
		}


		// テーブルに該当のデータが見つかった場合(ログイン成功時)                  19p ③
		// セッションにユーザの情報をセット   (今回セッション期間を設定してない為ずっと残り続ける)
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user); //setAttribute(String name, Object value);の型により"userInfo"はname部分

		// ユーザ一覧のサーブレットにリダイレクト   (リダイレクトはデータを持ち出せない)
		response.sendRedirect("UserListServlet");

	}

}








//5F4DCC3B5AA765D61D8327DEB882CF99 pswordのハッシュ値
