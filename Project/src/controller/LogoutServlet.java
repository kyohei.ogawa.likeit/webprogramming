package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/LogoutServlet")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public LogoutServlet() {
        super();
    }

    //getメソセッション 破棄する
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);

		//session.invalidate();    //セッション破棄       (全てのセッションを破棄)
		session.removeAttribute("userInfo");            //ログインした情報のみピンポイントを破棄する。上のinvalidate();は全ての情報を破棄
		response.sendRedirect("LoginServlet");   //ログインページへリダイレクト ※jspにリダイレクトはかけられない、(サーブレットでやりとりする事)



//		RequestDispatcher dispatch = request.getRequestDispatcher("/WEB-INF/jsp/web_exam.jsp");
//		dispatch.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
