package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserListServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		HttpSession session = request.getSession();
		User  loginsession = (User)session.getAttribute("userInfo");
		if (loginsession == null) {             //セッションが無ければログインページ(web_exam.jsp)にリダイレクト処理
			response.sendRedirect("LoginServlet");
			return;
		}



		userDao userDao = new userDao();
		List<User> userList = userDao.findall();

		request.setAttribute("userList", userList); //値をセット

		//ユーザー一覧(jsp)にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);
	}

	// ユーザー検索に使うメソッド
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		// UserList.jspの中の名前を呼ぶ(divのname部分)
		String id = request.getParameter("loginid"); //UserList.jspのloginid(jspのformの中のログインID)をidに初期化
		String name = request.getParameter("name"); //UserList.jspのname(ユーザー名)をnameに初期化
		String bd1 = request.getParameter("bd1"); //UserList.jspのbd1(誕生日1)を
		String bd2 = request.getParameter("bd2"); //UserList.jspのbd2(誕生日2)を

		userDao ud = new userDao(); //UserDaoクラスをインスタンス化

		List<User> serch = ud.serch(id, name, bd1, bd2); //userdaoのserchメソッドに引数を渡してserchで初期化

		request.setAttribute("userList", serch); //UserLIst.jspの中のuserListにserchをセット

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);

	}

}
