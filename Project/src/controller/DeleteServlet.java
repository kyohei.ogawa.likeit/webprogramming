package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userDao;
import model.User;


@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


    public DeleteServlet() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");     //字コード指定
		
		HttpSession session = request.getSession();
		User  loginsession = (User)session.getAttribute("userInfo");
		if (loginsession == null) {             //セッションが無ければログインページ(web_exam.jsp)にリダイレクト処理
			response.sendRedirect("LoginServlet");
			return;
		}
     //情報を持ってsakujokakuinin.jspに遷移する記述(updateサーブレットのdogetと同じ)
		String id = request.getParameter("id");     //userdaoメソ  インスタンス化

		int ID = Integer.parseInt(id);    //

		userDao ud = new userDao();

		User user =  ud.User_info(ID);

		request.setAttribute("name", user.getName());
		request.setAttribute("birthDate", user.getBirthDate());
		request.setAttribute("id", ID);

		RequestDispatcher dispatcher =request.getRequestDispatcher("/WEB-INF/jsp/UserSakujoKakunin.jsp");
		dispatcher.forward(request,response);



	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String ID = request.getParameter("id");               //データ受け取り

		int id = Integer.parseInt(ID);

		userDao UD = new userDao();

		User deleteUser = UD.Delite(id);      //削除メソ実行する

		response.sendRedirect("UserListServlet");

	}

}
