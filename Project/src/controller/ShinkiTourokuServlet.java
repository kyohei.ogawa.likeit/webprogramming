package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userDao;
import model.User;

/**
 * Servlet implementation class ShinkiTourokuServlet
 */
@WebServlet("/ShinkiTourokuServlet")
public class ShinkiTourokuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShinkiTourokuServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8"); //字コード指定(必ず指定)
		
		HttpSession session = request.getSession();
		User  loginsession = (User)session.getAttribute("userInfo");
		if (loginsession == null) {             //セッションが無ければログインページ(web_exam.jsp)にリダイレクト処理
			response.sendRedirect("LoginServlet");
			return;
		}
		//		UserShinkiTouroku.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserShinkiTouroku.jsp"); //遷移先を指定
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8"); //字化け防止コード

		String LoginId = request.getParameter("loginid"); //パラメーター受け取り
		String Pass1 = request.getParameter("password1"); //パスワード
		String Pass2 = request.getParameter("password2"); //確認パスワード
		String Name = request.getParameter("name");
		String B_D = request.getParameter("birthday"); 
		//equal()
                  //プログラムは上から動くから条件を最初に、条件をパスして通す処理は下に描く
		 if (!(Pass1.equals(Pass2))) { //パスワード違うなら新規登録画面のリクエストのerrMsgに入力された内容は正しくありませんを渡す

			request.setAttribute("errMsg", "入力が正しくされてません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserShinkiTouroku.jsp");
			dispatcher.forward(request, response);

			return;
		}

		 if(LoginId.equals("") || Pass1.equals("") ||Pass2.equals("") ||Name.equals("") ||B_D.equals("")) {   //から文字(何もない状態)

			request.setAttribute("errMsg", "情報が入力されていません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserShinkiTouroku.jsp");
			dispatcher.forward(request, response);
			request.setAttribute("Msg", "");
			return;
		}

		userDao userDao = new userDao();
		User NewUser = userDao.NewCostomor(LoginId, Pass1, Pass2, Name, B_D);
		response.sendRedirect("UserListServlet");
	}

}
