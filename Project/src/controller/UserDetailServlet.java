package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8"); //文字化け防止の記述(必ず書く)
		//web_exam.jspから送られてきたﾊﾟﾗﾒｰﾀｰ受け取る処理
		
		HttpSession session = request.getSession();
		User  loginsession = (User)session.getAttribute("userInfo");
		if (loginsession == null) {             //セッションが無ければログインページ(web_exam.jsp)にリダイレクト処理
			response.sendRedirect("LoginServlet");
			return;
		}

		String id = request.getParameter("id");     //reqestのidを受け取り、idとして初期化
	
		int ID = Integer.parseInt(id);            //idは元String、をint型にはめ直す。

		userDao UD = new userDao();           //userDao型(DAO)のuserDaoメソッドをインスタンス(UD)に初期化。

		User user = UD.User_info(ID);          //User(model)型のUser_infoメソにIDを持たしてUDと融合したものをuserで初期化。

		request.setAttribute("user", user);        //dogetメソの呼び出し元のuserに上で設定したuserを渡す。

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserJouhouShousaiSanshou.jsp");
		dispatcher.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
