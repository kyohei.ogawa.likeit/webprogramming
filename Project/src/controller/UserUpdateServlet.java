package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserUpdateServlet() {
		super();

	}

	//UserLIst.jspの更新ボタンを押した時このdogetにアクセスしてくる
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User  loginsession = (User)session.getAttribute("userInfo");
		if (loginsession == null) {             //セッションが無ければログインページ(web_exam.jsp)にリダイレクト処理
			response.sendRedirect("LoginServlet");
			return;
		}

		String id = request.getParameter("id"); //idの受け取り処理(userListの更新ボタンでidをuser.idにされてる 121行目)


		int ID = Integer.parseInt(id);             //String型をint型に変換

		userDao UD = new userDao(); //userDao型(DAO)のuserDaoメソッドをインスタンス(UD)に初期化。

		User user = UD.User_info(ID); //User(model)型のUser_infoメソにIDを持たしてUDと融合したものをuserで初期化。

		request.setAttribute("name", user.getName());
		request.setAttribute("birthDate", user.getBirthDate());
		request.setAttribute("id",id); // IDが入っていたところ

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserJouhouKoushin.jsp");
		dispatcher.forward(request, response);
	}



	//UserJouhouKoushin.jspから飛んでくるメソ
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String ID = request.getParameter("id");      //idにはlogin_idが入ってる(名前なるもの)
		String pass1 = request.getParameter("pass1");
		String pass2 = request.getParameter("pass2");
		String name = request.getParameter("name");     //nameにはユーザー名が入ってる(ニックネームの様なもの)
		String bd = request.getParameter("bd");
		//String UPdate = request.getParameter("update");
		int id = Integer.parseInt(ID);


		//パスワードが一致しなければ、passwordが一致しませんを赤枠の中に出力
		if (!(pass1.equals(pass2))) {
			request.setAttribute("errMsg",  "passwordが一致しません");
			request.setAttribute("id",id );
			request.setAttribute("name", name);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserJouhouKoushin.jsp");
			dispatcher.forward(request, response); //
			return;
		}
		//入力欄のどれかが空欄であればerrMsgをリクエストにセットして入力が不適切です、が出る。
		//if (ID.equals("") || pass1.equals("") || pass2.equals("") || name.equals("") || bd.equals("")) {   //どれかが空欄(一つでも) = エラー
			if (ID.equals("") || name.equals("") || bd.equals("")) {        //ID(),名前,誕生日が空ならエラー
		//if (ID.equals("") && name.equals("") && bd.equals("")) {           //IDもnameもbdも空ならエラー
			request.setAttribute("errMsg", "入力に不備があります");
			request.setAttribute("id",id );
			request.setAttribute("name", name);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserJouhouKoushin.jsp");
			dispatcher.forward(request, response);
//			request.setAttribute("errMsg", "");
			return;
		}
			if (pass1.equals("")) {

		userDao ud = new userDao();
		User UpdateUser = ud.Update(id, name, bd);

		request.setAttribute("id", id);
		request.setAttribute("name", name);
		request.setAttribute("bd", bd);

		response.sendRedirect("UserListServlet");

		}else {
		userDao UD = new userDao(); //成した場合のみ実行される
		User UpdateUser = UD.Update(id, name, bd, pass1);


		request.setAttribute("id", id);
		request.setAttribute("name", name);
		request.setAttribute("bd", bd);
		//request.setAttribute("up", UPdate);
		request.setAttribute("pass1", pass1);

		response.sendRedirect("UserListServlet");

		}

}}

